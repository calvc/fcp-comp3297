from fume.models import Member
from django.core.exceptions import ObjectDoesNotExist

def create_member(backend, user, response, *args, **kwargs):
    if backend.name == 'github':
        try:
            member = Member.objects.get(user=user)
        except ObjectDoesNotExist:
            member = Member(user=user,onScreenName=user.username)
        member.save()
