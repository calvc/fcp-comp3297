from django import forms
from .models import Member
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class MemberForm(UserCreationForm):
    onScreenName = forms.CharField(required=True,max_length=100)
    email = forms.EmailField(required=True)
    avatar = forms.ImageField(required=False)
    class Meta:
        model = User
        fields = ['username']
    def save(self, commit=True):
        user = super(MemberForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
