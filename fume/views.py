from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from fume.models import Member, Game, Genre, Review, Reward, Transaction
from django.template import Context
from django.contrib import auth
from django.http import Http404
import json
from .forms import MemberForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from datetime import datetime, timedelta
from taggit.models import Tag
from decimal import *


# handel ajax calls


# add tag via ajax
@csrf_exempt
def addtag(request):
    if request.is_ajax() and request.user.is_authenticated:
        tag = request.POST.get('tag', '')
        user = int(request.POST.get('userid', ''))
        game = int(request.POST.get('gameid', ''))
        if tag and user and game:
            user = User.objects.get(id=user)
            game = Game.objects.get(id=game)
            game.tags.add(tag)
            game.save()
            return HttpResponse('ok')
    return Http404()


# get json list of games for autocomplete
def games(request):
    if request.is_ajax():
        return HttpResponse(
            json.dumps(list(Game.objects.values('name', 'id'))))
    else:
        raise Http404("404 not found")


# Recommendations of games of interest are made on the basis of three target games – these are
# the three games most recently purchased by the member. For each target, similar games are
# identified in terms of the number of tags they share with the target. In each case, the most
# similar game – that is, the game that shares most tags with the target – that is not already
# present in the member’s purchase history is recommended. In the case where there are several
# best matches with a target, the game with the most recent release date is recommended. If
# several targets yield the same recommendation, then that recommendation is shown only once
# and, thus, fewer recommendations are made. If the member has purchased only two games,
# then a maximum of two recommendations can be made, and so on.

# render view
def index(request):
    featured = Game.objects.filter(isFeatured=True)
    recommended = None
    rewards = None
    genres = Genre.objects.all()
    tags = Tag.objects.all()
    currentSpending = None
    if request.user.is_authenticated():
        try:
            member = Member.objects.get(user=request.user)
            rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
            currentSpending = member.currentSpending
            lastest_perchased = [t.game for t in Transaction.objects.filter(member=member).order_by('-date')][:3]
            purchased = member.purchased.all()
            recommended = []
            for p in lastest_perchased:
                for s in p.tags.similar_objects():
                    if s not in purchased:
                        recommended.append(s)
                        break
            recommended = list(set(recommended))
        except Exception as e:
             print('%s (%s)' % (e, type(e)))
             return render(request, 'index.html', {
                 "featured": featured,
                 "recommended": recommended,
                 "genres": genres,
                 "tags": tags,
                 "spending": currentSpending,
                 "rewards":rewards
             })
    return render(request, 'index.html', {
        "featured": featured,
        "recommended": recommended,
        "genres": genres,
        "tags": tags,
        "spending": currentSpending,
        "rewards":rewards
    })


def tags(request):
    tags = Tag.objects.all()
    return render(request, 'tags.html', context=Context({"tags": tags}))


def tag(request, tag):
    genres = Genre.objects.all()
    tag = Tag.objects.get(name=tag)
    games = Game.objects.filter(tags__name__in=[tag,]).distinct()
    tags = Tag.objects.all()
    currentSpending = None
    rewards = None
    if request.user.is_authenticated():
        member = Member.objects.get(user=request.user)
        currentSpending = member.currentSpending
        rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
    return render(
        request,
        'tag.html',
        {
            "genres": genres,
            "tags": tags,
            "games": games,
            "thisTag": tag,
            "spending": currentSpending,
            "rewards": rewards
        })


def genres(request):
    genres = Genre.objects.all()
    return render(request, 'genres.html', context=Context({"genres": genres}))


def genre(request, genre):
    genres = Genre.objects.all()
    genre = Genre.objects.get(abbr=genre)
    games = genre.game_set.all()
    tags = Tag.objects.all()
    currentSpending = None
    rewards = None
    if request.user.is_authenticated():
        try:
            member = Member.objects.get(user=request.user)
            rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
            currentSpending = member.currentSpending
        except:
            pass
    return render(
        request,
        'genre.html',
        {
            "genres": genres,
            "tags": tags,
            "games": games,
            "thisGenre": genre,
            "spending": currentSpending,
            "rewards":rewards
        })


def details(request, gameId):
    game = Game.objects.get(id=gameId)
    genres = Genre.objects.all()
    tags = Tag.objects.all()
    gametags = game.tags.all()
    purchased = False
    currentSpending = None
    rewards = None
    if request.user.is_authenticated():
        try:
            member = Member.objects.get(user=request.user)
            currentSpending = member.currentSpending
            rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
            if game in member.purchased.all():
                purchased = True
        except:
            member = None
            currentSpending = None


    return render(
        request,
        'details.html',
        {
            "genres": genres,
            "tags": tags,
            "gametags": gametags,
            "game": game,
            "purchased": purchased,
            "spending": currentSpending,
            "rewards": rewards
        })


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    if not request.POST:
        return render(request, 'login.html')

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect(request.POST.get('next', '/'))
    else:
        return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    if request.POST:
        memberform = MemberForm(request.POST)
        if memberform.is_valid():
            user = memberform.save()
            auth.login(request, user,
                       'django.contrib.auth.backends.ModelBackend')
            onScreenName = memberform.cleaned_data['onScreenName']
            avatar = memberform.cleaned_data['avatar']
            member = Member(user=user, onScreenName=onScreenName)
            member.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'register.html', {'memberform': memberform})
    memberform = MemberForm()
    return render(request, 'register.html', {'memberform': memberform})


def forget(request):
    return render(request, 'forget.html', {forgetform: forgetform})

@login_required
def myProfile(request):
    member = Member.objects.get(user=request.user)

    currentSpending = member.currentSpending
    rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
    return render(request, 'myProfile.html',{"spending":currentSpending,"rewards":rewards})

@login_required
def myGames(request):
    genres = Genre.objects.all()
    tags = Tag.objects.all()
    member = Member.objects.get(user=request.user)
    rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
    currentSpending = member.currentSpending
    games = member.purchased.all()

    return render(request, 'mygames.html', {
        "games":games,
        "genres": genres,
        "tags": tags,
        "spending": currentSpending,
        "rewards" : rewards
    })


@login_required
def purchase(request, gameId):
    member = Member.objects.get(user=request.user)
    game = Game.objects.get(id=gameId)
    if not game:
        raise Http404("404 not found")
    if request.method == "GET":
        if game in member.purchased.all():
            return HttpResponseRedirect('/mygames')
        currentSpending = member.currentSpending
        rewards = member.reward_set.filter(is_active=True).filter(expiration_date__gte=datetime.now())
        return render(request, 'purchase.html', {
            'game': game,
            'spending': currentSpending,
            'rewards': rewards
        })
    elif request.POST:
        num = int(request.POST.get('rewards',0))
        rewards = Reward.objects.filter(owner=member).filter(is_active=True).filter(expiration_date__gte=datetime.now())
        rewards = sorted(rewards,key=lambda x: x.expiration_date)[:num]

        for r in rewards:
            r.is_active = False
            r.save()
        paid = game.price * Decimal( 1 - num * 0.1)
        Transaction.objects.create(member=member,game=game,paid=paid)
        total = paid + member.currentSpending
        issue_rewards(total,member)
        new_spending = (total) % 100
        member.currentSpending = new_spending
        member.save()
        return HttpResponseRedirect('/mygames')


def issue_rewards(total,member):
    num_of_reward = int(total / 100)
    for i in range(num_of_reward):
        reward = Reward()
        reward.owner = member
        reward.save()
