from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    # view
    url(r'^$', views.index, name="index"),
    url(r'^tags/(?P<tag>.+)$', views.tag, name="tag"),
    url(r'^genres/(?P<genre>\w+)$', views.genre, name="genre"),
    url(r'^games/(?P<gameId>\d+)$', views.details, name="details"),
    url(r'^login/$', views.login, name="login"),
    url(r'^logout/$', views.logout, name="logout"),
    url(r'^register/$', views.register, name="register"),
    url(r'^forget/$', views.forget, name="forget"),
    url(r'^myProfile/$', views.myProfile, name="myprofile"),
    url(r'^mygames/$', views.myGames, name="mygames"),
    url(r'^purchase/(?P<gameId>\d+)$', views.purchase, name="purchase"),
    url(r'^password_reset/$',
        auth_views.password_reset,
        {'template_name': 'password_reset_form.html'},
        name='password_reset'),
    url(r'^password_reset/done/$',
        auth_views.password_reset_done,
        {'template_name': 'password_reset_done.html'},
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {'template_name': 'password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^reset/done/$',
        auth_views.password_reset_complete,
        {'template_name': 'password_reset_complete.html'},
        name='password_reset_complete'),
    # ajax
    url(r'^games/$', views.games, name="games"),
    url(r'^addtag/$', views.addtag, name="addtag"),
    # oauth
    url(r'^oauth/', include('social_django.urls', namespace='social')),  # <--
]
