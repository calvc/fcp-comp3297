from django.contrib import admin
from fume.models import Member, Game, Genre, Review, Transaction, Reward

class GameAdmin(admin.ModelAdmin):
     list_filter = ('isFeatured',)

# Register your models here.
admin.site.register(Member)
admin.site.register(Game,GameAdmin)
admin.site.register(Genre)
admin.site.register(Reward)
admin.site.register(Review)
admin.site.register(Transaction)
