$(document).ready(function() {
    if (1) {

        $('.carousel.carousel-slider').carousel({fullWidth: true});

        autoplay();

        $.getJSON("../games", function(data) {
            var list = {}
            data.forEach(function(d) {
                // d.name = d.name + '(' + d.id + ')'
                list[d.name] = null;
            });


            $('input.autocomplete').autocomplete({
                data: list,
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                onAutocomplete: function(val) {
                    console.log(val);
                    window.location.href = '../games/2';
                },
                minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
            });
        });
    }

    $('select').material_select();

    $("#rewards").on('change', function() {
        num = $(this).val();
        org = parseFloat($('#price').text().substring(1))
        total = org * (1-(num * 0.1));
        if(num==0){
            total = org
        }
        $('#total').text('$'+total);
    });

    $("#savebtn").click(function(){
        alert("SAVED");
    });
    $('#addtag').click(function(){
        tag = $('#tag').val()
        gameid = $('#gameid').val()
        userid = $('#userid').val()
        if(tag){
            $.post("../addtag/",{'tag':tag,'gameid':gameid,'userid':userid} ,function(data){
                if(data == 'ok'){
                    $('#tag').val("")
                    location.reload();
                }
            });
        }else{
            alert("tag cannot be empty");
        }
    })
});

function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 3500);
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#avatar_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function() {
    readURL(this);
});
