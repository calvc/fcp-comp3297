from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from datetime import datetime, timedelta

class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(
        upload_to='members/', default='placeholder_profile.jpg')
    onScreenName = models.CharField(max_length=1024)
    currentSpending = models.DecimalField(default=33,max_digits=10,decimal_places=2)
    purchased = models.ManyToManyField('Game',through='Transaction',related_name='owned_by' ,blank=True,null=True)

    def __str__(self):
        return self.user.username

class Transaction(models.Model):
    member = models.ForeignKey('Member')
    game = models.ForeignKey('Game')
    paid = models.DecimalField(max_digits=10,decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)

class Game(models.Model):
    name = models.CharField(max_length=128)
    platform = models.CharField(max_length=64)
    description = models.TextField()
    icon = models.ImageField(
        upload_to='games/',
        default='placeholder_icon.jpg')
    header = models.ImageField(
        upload_to='games/',
        default='placeholder_header.jpg')
    price = models.DecimalField(max_digits=10,decimal_places=2)
    releaseDate = models.DateField()
    ownedBy = models.ManyToManyField(Member, blank=True)
    isFeatured = models.BooleanField(default=False)
    genre = models.ForeignKey('Genre', default=1)
    tags = TaggableManager(blank=True)
    def __str__(self):
        return self.name

class Reward(models.Model):
    is_active = models.BooleanField(default=True)
    expiration_date = models.DateTimeField(default=datetime.now()+timedelta(days=120))
    owner = models.ForeignKey('Member', on_delete=models.CASCADE, blank=True,null=True)

class Genre(models.Model):
    name = models.CharField(max_length=64)
    abbr = models.CharField(max_length=64)
    def __str__(self):
        return self.name


# class Tag(models.Model):
#     name = models.CharField(max_length=64)
#     creator = models.ForeignKey('Member', on_delete=models.CASCADE)
#     def __str__(self):
#         return self.name

class Review(models.Model):
    content = models.CharField(max_length=64)
    creator = models.ForeignKey('Member', on_delete=models.CASCADE)
    game = models.ForeignKey('Game', default=1)
